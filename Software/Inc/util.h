/**
  ******************************************************************************
  * @file    util.h
  * @author  TUT Robotics Club NPO
  * @version V1.0
  * @date    08-July-2013
  * @brief   This file contains definitions for Buttons control functions.
  ******************************************************************************  
  */ 
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __UTIL_H
#define __UTIL_H

#ifdef __cplusplus
 extern "C" {
#endif
   
/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"

/* Exported macros -----------------------------------------------------------*/
#ifndef UNUSED
#define UNUSED(x)            (void)(x)
#endif

#ifndef MIN
#define MIN(a, b)            (((a) < (b)) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(a, b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef ABS
#define ABS(x)               (((x) < 0) ? -(x) : (x))
#endif

#define SIGN(x)              (((x) < 0) ? -1 : 1)
#define LIMIT(x, min, max)   ((x) < (min) ? (min) : ((x) > (max) ? (max) : (x)))
#define ROUNDDIV(x, d)       (((x) + SIGN(x) * ((d) / 2)) / (d))
#define SWAP(type, i, j)     { type t = i; i = j; j = t; }
#define COUNT_OF(array)      (sizeof(array) / sizeof(array[0]))
#define BITMASK(b)           (1UL << (U32)(b))
#define SET_BITMASK(v, b)    (v) |= (U32) BITMASK(b)
#define CLR_BITMASK(v, b)    (v) &= (U32)~BITMASK(b)

/* Public constants ----------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern Bool Util_Countdown(U32 * Timer, U32 Period, Bool DetectEdge);
extern void Util_CreateDateTime(DateTime * Dt, U16 Year, U8 Month, U8 Day, U8 Hour, U8 Minute, U8 Second);
extern S32  Util_CompareDateTime(DateTime * A, DateTime * B);
extern U16  Util_BytesToUInt16(const U8 * Array);
extern U32  Util_BytesToUInt32(const U8 * Array);
extern U64  Util_BytesToUInt64(const U8 * Array);
extern void Util_UInt16ToBytes(U16 Value, U8 * Array);
extern void Util_UInt32ToBytes(U32 Value, U8 * Array);
extern void Util_UInt64ToBytes(U64 Value, U8 * Array);
extern void Util_ASCIIToHex(const char * Text, char * HexBuffer, U32 HexBufferSize);
extern void Util_HexToASCII(const char * HexBuffer, char * Text, U32 TextSize);

#ifdef __cplusplus
}
#endif

#endif
