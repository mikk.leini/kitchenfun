/**
  ******************************************************************************
  * @file    bytefifo.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    10-June-2013
  * @brief   This file contains definitions for byte FIFO.
  ******************************************************************************  
  */ 
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BYTEFIFO_H
#define __BYTEFIFO_H

#ifdef __cplusplus
 extern "C" {
#endif
   
/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
#include "stm32l0xx.h"

/* Undef macros for FIFO -----------------------------------------------------*/
#ifdef FIFO_NAME
#undef FIFO_NAME
#endif

#ifdef FIFO_TYPE
#undef FIFO_TYPE
#endif

#ifdef FIFO_FUNC
#undef FIFO_FUNC
#endif

#ifdef FIFO_MUTEX_TYPE
#undef FIFO_MUTEX_TYPE
#endif

#ifdef FIFO_ATOMIC_START
#undef FIFO_ATOMIC_START
#endif

#ifdef FIFO_ATOMIC_END
#undef FIFO_ATOMIC_END
#endif

/* Macros for FIFO -----------------------------------------------------------*/
#define FIFO_NAME           ByteFifo
#define FIFO_TYPE           U8
#define FIFO_FUNC(suffix)   ByteFifo_ ## suffix
#define FIFO_ATOMIC_START   __disable_irq();
#define FIFO_ATOMIC_END     __enable_irq();
   
/* Include the real code */
#include "fifo.h"

#ifdef __cplusplus
}
#endif

#endif
