/**
  ******************************************************************************
  * @file    fifo.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    22-July-2013
  * @brief   This file contains generic FIFO functions. This file has to be
  *          included from some source file.
  ******************************************************************************  
  */

/* Define to prevent recursive inclusion -------------------------------------*/

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"

/* Public types --------------------------------------------------------------*/
typedef struct
{
  FIFO_TYPE * Data;
  U32 Capacity;
  U32 Length;
  U32 ReadIndex;
  U32 WriteIndex;
  U32 PeekDepth;
#ifdef FIFO_MUTEX_TYPE
  FIFO_MUTEX_TYPE Mutex;
#endif

} FIFO_NAME;

/* Public macros -------------------------------------------------------------*/

/* Public function declarations ----------------------------------------------*/
extern void      FIFO_FUNC(Init)          (FIFO_NAME * Fifo, FIFO_TYPE * Data, U32 Capacity);
extern void      FIFO_FUNC(Clear)         (FIFO_NAME * Fifo);
extern Bool      FIFO_FUNC(IsEmpty)       (FIFO_NAME * Fifo);
extern Bool      FIFO_FUNC(IsFull)        (FIFO_NAME * Fifo);
extern U32       FIFO_FUNC(GetUsedSpace)  (FIFO_NAME * Fifo);
extern U32       FIFO_FUNC(GetFreeSpace)  (FIFO_NAME * Fifo);
extern Bool      FIFO_FUNC(Push)          (FIFO_NAME * Fifo, FIFO_TYPE Item);
extern Bool      FIFO_FUNC(PushPtr)       (FIFO_NAME * Fifo, const FIFO_TYPE * Item);
extern Bool      FIFO_FUNC(PushBegin)     (FIFO_NAME * Fifo, FIFO_TYPE ** Item);
extern void      FIFO_FUNC(PushEnd)       (FIFO_NAME * Fifo);
extern Bool      FIFO_FUNC(Pop)           (FIFO_NAME * Fifo, FIFO_TYPE * Item);
extern Bool      FIFO_FUNC(PopBegin)      (FIFO_NAME * Fifo, FIFO_TYPE ** Item);
extern void      FIFO_FUNC(PopEnd)        (FIFO_NAME * Fifo);
extern Bool      FIFO_FUNC(Front)         (FIFO_NAME * Fifo, FIFO_TYPE * Item);
extern Bool      FIFO_FUNC(Back)          (FIFO_NAME * Fifo, FIFO_TYPE * Item);
extern Bool      FIFO_FUNC(Peek)          (FIFO_NAME * Fifo, FIFO_TYPE * Item);
extern void      FIFO_FUNC(RestartPeek)   (FIFO_NAME * Fifo);
extern void      FIFO_FUNC(FlushAllPeeked)(FIFO_NAME * Fifo);
