/**
  ******************************************************************************
  * @file    ui.h
  * @brief   This file contains definitions for user interface functions.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __UI_H
#define __UI_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define NUM_BUTTONS        5
#define NUM_POTENTIOMETERS 3

/* Exported functions --------------------------------------------------------*/
extern void UI_Init(void);
extern void UI_Task(void);
extern Bool UI_GetButtonState(U8 button);
extern Bool UI_GetButtonPress(U8 button);
extern void UI_SetButtonLED(U8 button, Bool onOff);
extern void UI_SetPowerLED(Bool onOff);
extern void UI_SetAuxLED(Bool onOff);
extern void UI_SetRGBLED(U8 red, U8 green, U8 blue);
extern U16  UI_GetPotentiometer(U8 channel);

#ifdef __cplusplus
}
#endif

#endif
