/**
  ******************************************************************************
  * @file    dfplayer.h
  * @brief   This file contains functions for controlling DFPLayer mini
  * @details Based on these source codes:
  *          https://github.com/DFRobot/DFPlayer-Mini-mp3
  *          https://github.com/PowerBroker2/DFPlayerMini_Fast
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef DFPLAYER
#define DFPLAYER

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"

/* Public function declarations ----------------------------------------------*/
extern void DFPlayer_Init();
extern void DFPlayer_Task();
extern void DFPlayer_SetVolume(U8 volume);
extern void DFPlayer_Play(U16 file);

#ifdef __cplusplus
 extern "C" {
#endif

#endif
