/**
  ******************************************************************************
  * @file    typedefs.h
  * @author  Mikk Leini
  * @version V1.0
  * @date    08-July-2013
  * @brief   This file contains root type definitions.
  ******************************************************************************  
  */ 
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TYPEDEFS_H
#define __TYPEDEFS_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>

/* Default parameter types */
typedef uint32_t  Bool;
typedef uint8_t   U2;
typedef uint8_t   U4;
typedef uint8_t   U8;
typedef int8_t    S8;
typedef uint16_t  U16;
typedef int16_t   S16;
typedef uint32_t  U32;
typedef int32_t   S32;
typedef uint64_t  U64;
typedef int64_t   S64;
typedef float     Float;
typedef double    Double;

/* 32 bits of bit-packed date and time */
typedef struct
{
  U8 Year   : 6; /* 0 to 63 + year 2000 = 2000 to 2063 */
  U8 Month  : 4; /* 1 to 12 */
  U8 Day    : 5; /* 1 to 31 */
  U8 Hour   : 5; /* 0 to 23 */
  U8 Minute : 6; /* 0 to 59 */
  U8 Second : 6; /* 0 to 60 (leap second) */
  
} DateTime;

/* Asynchronous operation result */
typedef enum
{
  AsyncFalse,
  AsyncTrue,
  AsyncBusy
  
} AsyncResult;

/* Exported macros ----------------------------------------------------------*/

/* Can't use True and False because they already exist in STM32 USB library */
#define True  1U
#define False 0U

#ifndef NULL
#define NULL 0
#endif
   
#ifdef __cplusplus
}
#endif

#endif
