/*
 * game.h
 *
 *  Created on: 28. dets 2018
 *      Author: Mikk
 */

#ifndef GAME_H_
#define GAME_H_

extern void Game_Init(void);
extern void Game_Task(void);

#endif /* GAME_H_ */
