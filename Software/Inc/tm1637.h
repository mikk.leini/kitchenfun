/**
  ******************************************************************************
  * @file    tm1637.h
  * @brief   This file contains functions for controlling TM1637 segment display
  * @details Based on this source code:
  *          https://github.com/avishorp/TM1637
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef TM1637
#define TM1637

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

/* Includes ------------------------------------------------------------------*/
#include "inttypes.h"

/* Exported constants --------------------------------------------------------*/
#define SEG_A   0b00000001
#define SEG_B   0b00000010
#define SEG_C   0b00000100
#define SEG_D   0b00001000
#define SEG_E   0b00010000
#define SEG_F   0b00100000
#define SEG_G   0b01000000

extern const uint8_t TM1637_HexDigitToSegment[16];
extern const uint8_t TM1637_OrbitToSegments[12][4];
extern const uint8_t TM1637_BounceToSegments[6][4];

 /* Public function declarations ----------------------------------------------*/
extern void    TM1637_Init();
extern void    TM1637_SetBrightness(uint8_t brightness, uint8_t on);
extern void    TM1637_SetSegments(const uint8_t segments[], uint8_t length, uint8_t pos);
extern void    TM1637_Clear();
extern void    TM1637_ShowNumberDec(int num, uint8_t leading_zero, uint8_t length, uint8_t pos);
extern void    TM1637_ShowNumberDecEx(int num, uint8_t dots, uint8_t leading_zero, uint8_t length, uint8_t pos);
extern void    TM1637_ShowNumberHexEx(uint16_t num, uint8_t dots, uint8_t leading_zero, uint8_t length, uint8_t pos);

#ifdef __cplusplus
}
#endif

#endif
