/**
  ******************************************************************************
  * @file    ui.c
  * @brief   This file contains user interface functions.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
#include <string.h>
#include "stm32l0xx.h"
#include "main.h"
#include "ui.h"

/* Private constant ----------------------------------------------------------*/
const static struct
{
  GPIO_TypeDef * btn_port;
  U16            btn_pin;
  GPIO_TypeDef * led_port;
  U16            led_pin;
}
buttons[NUM_BUTTONS] =
{
  { BL1_BTN_GPIO_Port, BL1_BTN_Pin, BL1_LED_GPIO_Port, BL1_LED_Pin },
  { BL2_BTN_GPIO_Port, BL2_BTN_Pin, BL2_LED_GPIO_Port, BL2_LED_Pin },
  { BL3_BTN_GPIO_Port, BL3_BTN_Pin, BL3_LED_GPIO_Port, BL3_LED_Pin },
  { BL4_BTN_GPIO_Port, BL4_BTN_Pin, BL4_LED_GPIO_Port, BL4_LED_Pin },
  { BL5_BTN_GPIO_Port, BL5_BTN_Pin, BL5_LED_GPIO_Port, BL5_LED_Pin },
  /* Not mounted:
  { BL6_BTN_GPIO_Port, BL6_BTN_Pin, BL6_LED_GPIO_Port, BL6_LED_Pin } */
};

#define BUTTON_MASK  0x0F

/* Private macro -------------------------------------------------------------*/
/* Private type --------------------------------------------------------------*/
/* Public variables ---------------------------------------------------------*/
extern TIM_HandleTypeDef htim2;
extern ADC_HandleTypeDef hadc;

/* Private variables ---------------------------------------------------------*/
static U32  button_samples[NUM_BUTTONS];
static Bool button_pressed[NUM_BUTTONS];
static Bool button_reported[NUM_BUTTONS];
static U16  adc_samples[NUM_POTENTIOMETERS + 1]; /* Make sure it aligns to 32-bits */
static Bool adc_started;

/* Private function declarations ---------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Public functions ----------------------------------------------------------*/

/**
  * @brief  User interface initialization.
  */
void UI_Init(void)
{
  adc_started = False;
  memset((void *)adc_samples, 0, sizeof(adc_samples));

  /* Compare levels are zero, start PWM */
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4);
}

/**
  * @brief  User interface task
  */
void UI_Task(void)
{
  U32 i;

  /* If ADC is started then check if it's completed */
  if (adc_started)
  {
    if ((HAL_ADC_GetState(&hadc) & HAL_ADC_STATE_REG_EOC) != 0)
    {
      adc_started = False;
    }
  }

  /* If ADC is not started then start */
  if (!adc_started)
  {
    if (HAL_ADC_Start_DMA(&hadc, (void *)adc_samples, NUM_POTENTIOMETERS) == HAL_OK)
    {
      adc_started = True;
    }
  }

  /* Sample buttons */
  for (i = 0; i < NUM_BUTTONS; i++)
  {
    button_samples[i] <<= 1;

    if (HAL_GPIO_ReadPin(buttons[i].btn_port, buttons[i].btn_pin) == GPIO_PIN_SET)
    {
      button_samples[i] |= 1;
    }

    /* Detect press and unpress */
    if ((button_pressed[i] == False) && ((button_samples[i] & BUTTON_MASK) == BUTTON_MASK))
    {
      button_pressed[i]  = True;
      button_reported[i] = False;
    }
    else if ((button_pressed[i] == True) && ((button_samples[i] & BUTTON_MASK) == 0))
    {
      button_pressed[i]  = False;
      button_reported[i] = False;
    }
  }
}

/**
 * @brief  Get button state
 * @param  button: Button index
 * @retval True if button is pressed, False if not
 */
Bool UI_GetButtonState(U8 button)
{
  assert_param(button < NUM_BUTTONS);

  if ((button_samples[button] & BUTTON_MASK) == BUTTON_MASK)
  {
    return True;
  }
  else
  {
    return False;
  }
}

/**
 * @brief  Get button press (once per press)
 * @param  button: Button index
 * @retval True if button is pressed, False if not
 */
Bool UI_GetButtonPress(U8 button)
{
  Bool value = False;
  assert_param(button < NUM_BUTTONS);

  if (button_reported[button] == 0)
  {
    value = button_pressed[button];
    button_reported[button] = True;
  }

  return value;
}

/**
 * @brief  Set button LED on or off
 * @param  button: Button index
 * @param  onOff: On (non-zero) or off (zero)
 */
void UI_SetButtonLED(U8 button, Bool onOff)
{
  assert_param(button < NUM_BUTTONS);

  HAL_GPIO_WritePin(buttons[button].led_port, buttons[button].led_pin,
      (onOff == True ? GPIO_PIN_SET : GPIO_PIN_RESET));
}

/**
 * @brief  Set power button LED on or off
 * @param  onOff: On (non-zero) or off (zero)
 */
void UI_SetPowerLED(Bool onOff)
{
  HAL_GPIO_WritePin(PWR_LED_GPIO_Port, PWR_LED_Pin,
      (onOff == True ? GPIO_PIN_SET : GPIO_PIN_RESET));
}

/**
 * @brief  Set auxiliary LED (LED1) on or off
 * @param  onOff: On (non-zero) or off (zero)
 */
void UI_SetAuxLED(Bool onOff)
{
  HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin,
      (onOff == True ? GPIO_PIN_SET : GPIO_PIN_RESET));
}

/**
 * @brief  Set RGB LED color
 * @param  red:   Intensity from 0 to 255 (max)
 * @param  green: Intensity from 0 to 255 (max)
 * @param  blue:  Intensity from 0 to 255 (max)
 */
void UI_SetRGBLED(U8 red, U8 green, U8 blue)
{
  __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, red);
  __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, green);
  __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, blue);
}

/**
 * @brief  Get potentiometer value
 * @param  channel: Potentiometer channel
 * @retval Value from 0 to 4095
 */
U16 UI_GetPotentiometer(U8 channel)
{
  assert_param(channel < NUM_POTENTIOMETERS);

  return adc_samples[channel];
}
