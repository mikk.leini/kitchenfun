/**
  ******************************************************************************
  * @file    bytefifo.c
  * @author  Mikk Leini
  * @version V1.0
  * @date    22-July-2013
  * @brief   This file contains byte FIFO functions.
  ******************************************************************************  
  */

/* Includes ------------------------------------------------------------------*/
#include "bytefifo.h"

/* Include the real code */
#include "fifo.c"
