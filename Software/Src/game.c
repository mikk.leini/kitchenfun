/*
 * game.c
 *
 *  Created on: 28. dets 2018
 *      Author: Mikk
 */

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include "stm32l0xx_hal.h"
#include "ui.h"
#include "dfplayer.h"
#include "tm1637.h"
#include "game.h"

/* Private constants ---------------------------------------------------------*/
#define GAME_PERIOD   10 /* Task period in milliseconds */
#define NUM_SOUNDS    10

/* Buttons by color */
typedef enum
{
  BTN_BLUE   = 0,
  BTN_WHITE  = 1,
  BTN_YELLOW = 2,
  BTN_GREEN  = 3,
  BTN_RED    = 4
} Button;

/* Sounds by file ID's */
typedef enum
{
  SOUND_LETS_PLAY,
  SOUND_LETS_COOK_SOUP,
  SOUND_LETS_MAKE_PANCAKES,
  SOUND_LETS_COOK_EGGS,
  SOUND_LETS_MAKE_CAKE_WITH_JAM,
  SOUND_WOULD_LIKE_TO_COOK_SOME_FOOD,
  SOUND_LETS_MAKE_PORRIDGE,
  SOUND_CAKE_IS_BURNING,
  SOUND_SOUP_IS_OVERBOILING,
  SOUND_PANCAKES_ARE_BURNING,
  SOUND_WHERE_ARE_BANANAS,
  SOUND_ONE,
  SOUND_TWO,
  SOUND_THREE,
  SOUND_FOUR,
  SOUND_FIVE,
  SOUND_SIX,
  SOUND_SEVEN,
  SOUND_EIGHT,
  SOUND_NINE,
  SOUND_TEN,
  SOUND_WHERE_ARE_YOU,
  SOUND_WHERE_ARE_YOU_2,
  SOUND_DONT_PRESS_SO_MUCH_BUTTONS,
  SOUND_PRESS_BLUE_BUTTON,
  SOUND_PRESS_WHITE_BUTTON,
  SOUND_PRESS_YELLOW_BUTTON,
  SOUND_PRESS_GREEN_BUTTON,
  SOUND_PRESS_RED_BUTTON,
  SOUND_TURN_BLACK_KNOB,
  SOUND_WELL_DONE,
  SOUND_WELL_DONE_2,
  SOUND_WELL_DONE_3,
  SOUND_PRESS_AGAIN,
  SOUND_PRESS_GREEN_BUTTON_TO_START_OVEN,
  SOUND_PRESS_YELLOW_BUTTON_TO_STOP_OVEN,
  SOUND_DONT_FORGET_OVEN_ON,
  SOUND_METAL_PAN_CLANG,
  SOUND_METAL_POT_CLANG,
  SOUND_METAL_PONG,
  SOUND_EGG_CRACK,
  SOUND_WATER_DROP
} Sound;

typedef enum
{
  MODE_IDLE,
  MODE_COUNT_NUMBERS,
  MODE_ASK_PRESSES,
  MODE_SWITCH_COLORS
} Mode;


/* Private variables ---------------------------------------------------------*/
static struct
{
  Mode mode;
  U32 abstime;
  U32 timeout;

  struct
  {
    U32 effect;
  } idle;

  struct
  {
    U32 button;
    U32 counter;
    U32 timer;
  } counting;

  struct
  {
    U32 button;
    U32 score;
    U32 timer;
  } asking;

  struct
  {
    U16 avg;
    U32 timer;
  } knob;

} game;

/**
 * @brief  Initialize game
 */
void Game_Init(void)
{
  U32 i;

  /* Reset variables */
  game.mode    = MODE_IDLE;
  game.abstime = 0;
  game.timeout = 0;

  /* Light up all button LEDs */
  UI_SetPowerLED(True);
  for (i = 0; i < NUM_BUTTONS; i++)
  {
    UI_SetButtonLED(i, True);
  }

  /* Pick sound volume (from 0 to 30) */
  DFPlayer_SetVolume(20);

  /* Set LED display brightness */
  TM1637_SetBrightness(5, 1);

  /* Clear LED display */
  TM1637_Clear();

  /* Say hello */
  DFPlayer_Play(SOUND_LETS_PLAY + rand() % (SOUND_LETS_MAKE_PORRIDGE - SOUND_LETS_PLAY));
}

/**
 * @brief  Periodic game task (10 ms period)
 */
void Game_Task(void)
{
  U32 i;
  S32 pressedButton = -1;
  U16 knob;
  S16 knobDiff;

  /* Count absolute time */
  game.abstime += GAME_PERIOD;

  /* Count timeout */
  if (game.timeout > GAME_PERIOD)
  {
    game.timeout -= GAME_PERIOD;
  }
  else
  {
    game.timeout = 0;
  }

  /* Check buttons (once at time) */
  for (i = 0; i < NUM_BUTTONS; i++)
  {
    if (UI_GetButtonPress(i))
    {
      pressedButton = i;
      break;
    }
  }

  /* Get knob */
  knob = UI_GetPotentiometer(2);
  game.knob.avg = (knob + 3 * game.knob.avg) / 4;
  knobDiff = (S16)knob - (S16)game.knob.avg;

  /* Which mode ? */
  switch (game.mode)
  {
    case MODE_IDLE:

      /* Display orbiting segment */
      TM1637_SetSegments(TM1637_OrbitToSegments[(game.idle.effect / 10) % 12], 4, 0);
      game.idle.effect++;

      /* Light up all button LEDs */
      UI_SetPowerLED(True);
      for (i = 0; i < NUM_BUTTONS; i++)
      {
        UI_SetButtonLED(i, True);
      }

      /* Some button pressed ? */
      if (pressedButton >= 0)
      {
        /* Randomly pick new mode if button was pressed */
        switch (rand() % 5)
        {
          /* Numbers counting mode */
          case 0:

            /* Prepare for counting number of same button presses */
            game.mode             = MODE_COUNT_NUMBERS;
            game.counting.button  = pressedButton;
            game.counting.counter = 1;
            game.counting.timer   = 0;

            /* Say and display count */
            DFPlayer_Play(SOUND_ONE - 1 + game.counting.counter);
            TM1637_ShowNumberDec(game.counting.counter, 0, 4, 0);
            break;

          case 1:

            /* Prepare for asking pressing buttons by color */
            game.mode          = MODE_ASK_PRESSES;
            game.asking.button = rand() % NUM_BUTTONS;
            game.asking.score  = 1;
            game.asking.timer  = 0;

            /* Display score and ask to press a new button */
            TM1637_ShowNumberDec(game.asking.score, 0, 4, 0);
            DFPlayer_Play(SOUND_PRESS_BLUE_BUTTON + game.asking.button);
            break;

          default:

            /* Just say something random */
            DFPlayer_Play(SOUND_LETS_PLAY + rand() % (SOUND_WHERE_ARE_BANANAS - SOUND_LETS_PLAY));
            break;
        }
      }

      /* Knob turned ? Don't trust the diff at startup */
      if ((game.abstime > 500) && (abs(knobDiff) >= 15))
      {
        game.mode = MODE_SWITCH_COLORS;
        game.knob.timer = 0;
      }
      break;

    /* Counting numbers */
    case MODE_COUNT_NUMBERS:

      /* Count time */
      game.counting.timer += GAME_PERIOD;

      /* Button pressed and count not yet "10" ? */
      if ((pressedButton >= 0) && (game.counting.counter < 10))
      {
        /* Same button as supposed ? */
        if (pressedButton == game.counting.button)
        {
          /* Increase count and reset timer */
          game.counting.counter++;
          game.counting.timer = 0;

          /* Say and display the number */
          DFPlayer_Play(SOUND_ONE - 1 + game.counting.counter);
          TM1637_ShowNumberDec(game.counting.counter, 0, 4, 0);
        }
        else
        {
          /* Not the same button */
          DFPlayer_Play(SOUND_METAL_PAN_CLANG);

          /* Go idle */
          TM1637_Clear();
          game.mode = MODE_IDLE;
        }
      }
      /* Reached "10" ? */
      else if ((game.counting.counter >= 10) && (game.counting.timer >= 2000))
      {
        /* Go idle */
        TM1637_Clear();
        game.mode = MODE_IDLE;
      }
      /* Inactivity ? */
      else if (game.counting.timer == 5000)
      {
        /* Ask where are you ? */
        DFPlayer_Play(SOUND_WHERE_ARE_YOU + rand() % 2);
      }
      /* Timeout ? */
      else if (game.counting.timer == 10000)
      {
        /* Go idle */
        TM1637_Clear();
        game.mode = MODE_IDLE;
      }
      break;

    /* Ask for button presses */
    case MODE_ASK_PRESSES:

      game.asking.timer += GAME_PERIOD;

      /* Check for button presses */
      if (pressedButton >= 0)
      {
        /* Is it the button which was asked to press ? */
        if (pressedButton == game.asking.button)
        {
          /* Increase score */
          game.asking.score++;
          TM1637_ShowNumberDec(game.asking.score, 0, 4, 0);
          DFPlayer_Play(SOUND_WELL_DONE + rand() % 3);

          /* Ask to press a new button */
          game.asking.button = rand() % 5;
          game.asking.timer  = 0;
          DFPlayer_Play(SOUND_PRESS_BLUE_BUTTON + game.asking.button);
        }
        else
        {
          /* Decrease score */
          if (game.asking.score > 0) game.asking.score--;
          TM1637_ShowNumberDec(game.asking.score, 0, 4, 0);
          DFPlayer_Play(SOUND_METAL_PAN_CLANG);

          /* Ask to press a new button */
          game.asking.button = rand() % NUM_BUTTONS;
          game.asking.timer  = 0;
          DFPlayer_Play(SOUND_PRESS_BLUE_BUTTON + game.asking.button);
        }
      }
      /* Inactivity ? */
      else if (game.asking.timer == 5000)
      {
        /* Ask where are you ? */
        DFPlayer_Play(SOUND_WHERE_ARE_YOU + rand() % 2);
      }
      /* Timeout ? */
      else if (game.asking.timer == 10000)
      {
        /* Go idle */
        TM1637_Clear();
        game.mode = MODE_IDLE;
      }
      break;

    case MODE_SWITCH_COLORS:
      {
        /* Calculate knob position over all button LEDs */
        U16 knobPercent   = (knob / 40);
        U16 knobNrButton  = (knob * (NUM_BUTTONS + 1)) / 4096;

        /* Display percentage */
        TM1637_ShowNumberDec(knobPercent, 0, 4, 0);

        /* Turn on only one LED dependent on knob position */
        UI_SetPowerLED(knobNrButton == 0);
        for (i = 0; i < NUM_BUTTONS; i++)
        {
          UI_SetButtonLED(i, knobNrButton == (1 + i));
        }

        /* Count inactivity time */
        if (abs(knobDiff) < 15)
        {
          game.knob.timer += GAME_PERIOD;
          if (game.knob.timer >= 2000)
          {
            game.mode = MODE_IDLE;
          }
        }
        else
        {
          game.knob.timer = 0;
        }
      }
      break;
  }
}
