/**
  ******************************************************************************
  * @file    tm1637.c
  * @brief   This file contains functions for controlling TM1637 segment display
  * @details Based on this source code:
  *          https://github.com/avishorp/TM1637
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "tm1637.h"
#include "stm32l0xx.h"
#include "main.h"

/* Private constant ----------------------------------------------------------*/
#define TM1637_I2C_COMM1    0x40
#define TM1637_I2C_COMM2    0xC0
#define TM1637_I2C_COMM3    0x80

//      A
//     ---
//  F |   | B
//     -G-
//  E |   | C
//     ---
//      D

const uint8_t TM1637_HexDigitToSegment[16] =
{
 // XGFEDCBA
  0b00111111,    // 0
  0b00000110,    // 1
  0b01011011,    // 2
  0b01001111,    // 3
  0b01100110,    // 4
  0b01101101,    // 5
  0b01111101,    // 6
  0b00000111,    // 7
  0b01111111,    // 8
  0b01101111,    // 9
  0b01110111,    // A
  0b01111100,    // b
  0b00111001,    // C
  0b01011110,    // d
  0b01111001,    // E
  0b01110001     // F
};

//      A
//     ---
//  F |   | B
//     -G-
//  E |   | C
//     ---
//      D

const uint8_t TM1637_OrbitToSegments[12][4] =
{
  { SEG_A, 0, 0, 0 },
  { 0, SEG_A, 0, 0 },
  { 0, 0, SEG_A, 0 },
  { 0, 0, 0, SEG_A },
  { 0, 0, 0, SEG_B },
  { 0, 0, 0, SEG_C },
  { 0, 0, 0, SEG_D },
  { 0, 0, SEG_D, 0 },
  { 0, SEG_D, 0, 0 },
  { SEG_D, 0, 0, 0 },
  { SEG_E, 0, 0, 0 },
  { SEG_F, 0, 0, 0 }
};

//      A
//     ---
//  F |   | B
//     -G-
//  E |   | C
//     ---
//      D

const uint8_t TM1637_BounceToSegments[6][4] =
{
  { SEG_G, 0, 0, 0 },
  { 0, SEG_G, 0, 0 },
  { 0, 0, SEG_G, 0 },
  { 0, 0, 0, SEG_G },
  { 0, 0, SEG_G, 0 },
  { 0, SEG_G, 0, 0 },
};

static const uint8_t minusSegments = 0b01000000;

/* Private macros ------------------------------------------------------------*/
#define SCL(l) HAL_GPIO_WritePin(DISP_SCL_GPIO_Port, DISP_SCL_Pin, l)
#define SDA(l) HAL_GPIO_WritePin(DISP_SDA_GPIO_Port, DISP_SDA_Pin, l)

/* Private variables ---------------------------------------------------------*/
static uint8_t m_brightness;

/* Private function declarations ---------------------------------------------*/
static void    BitDelay();
static void    Start();
static void    Stop();
static uint8_t WriteByte(uint8_t b);
static void    ShowDots(uint8_t dots, uint8_t* digits);
static void    ShowNumberBaseEx(int8_t base, uint16_t num, uint8_t dots, uint8_t leading_zero, uint8_t length, uint8_t pos);

/* Private functions ---------------------------------------------------------*/
/* Public functions ----------------------------------------------------------*/

void TM1637_Init()
{
  // Release pins (high)
  SCL(1);
  SDA(1);
}

void TM1637_SetBrightness(uint8_t brightness, uint8_t on)
{
  m_brightness = (brightness & 0x7) | (on ? 0x08 : 0x00);
}

void TM1637_SetSegments(const uint8_t segments[], uint8_t length, uint8_t pos)
{
  // Write COMM1
  Start();
  WriteByte(TM1637_I2C_COMM1);
  Stop();

  // Write COMM2 + first digit address
  Start();
  WriteByte(TM1637_I2C_COMM2 + (pos & 0x03));

  // Write the data bytes
  for (uint8_t k = 0; k < length; k++)
  {
    WriteByte(segments[k]);
  }

  Stop();

  // Write COMM3 + brightness
  Start();
  WriteByte(TM1637_I2C_COMM3 + (m_brightness & 0x0f));
  Stop();
}

void TM1637_Clear()
{
  uint8_t data[] = { 0, 0, 0, 0 };
  TM1637_SetSegments(data, 4, 0);
}

void TM1637_ShowNumberDec(int num, uint8_t leading_zero, uint8_t length, uint8_t pos)
{
  TM1637_ShowNumberDecEx(num, 0, leading_zero, length, pos);
}

void TM1637_ShowNumberDecEx(int num, uint8_t dots, uint8_t leading_zero, uint8_t length, uint8_t pos)
{
  ShowNumberBaseEx(num < 0 ? -10 : 10, num < 0 ? -num : num, dots, leading_zero, length, pos);
}

void TM1637_ShowNumberHexEx(uint16_t num, uint8_t dots, uint8_t leading_zero, uint8_t length, uint8_t pos)
{
  ShowNumberBaseEx(16, num, dots, leading_zero, length, pos);
}

void ShowNumberBaseEx(int8_t base, uint16_t num, uint8_t dots, uint8_t leading_zero, uint8_t length, uint8_t pos)
{
  uint8_t negative = 0;

  if (base < 0)
  {
    base = -base;
    negative = 1;
  }

  uint8_t digits[4];

  if (num == 0 && !leading_zero)
  {
    // Singular case - take care separately
    for (uint8_t i = 0; i < (length - 1); i++)
      digits[i] = 0;

    digits[length - 1] = TM1637_HexDigitToSegment[0];
  }
  else
  {
    //uint8_t i = length-1;
    //if (negative) {
    //  // Negative number, show the minus sign
    //    digits[i] = minusSegments;
    //  i--;
    //}

    for (int i = length - 1; i >= 0; --i)
    {
      uint8_t digit = num % base;

      if (digit == 0 && num == 0 && leading_zero == 0)
        // Leading zero is blank
        digits[i] = 0;
      else
        digits[i] = TM1637_HexDigitToSegment[digit % 0xF];

      if (digit == 0 && num == 0 && negative)
      {
        digits[i] = minusSegments;
        negative = 0;
      }

      num /= base;
    }

    if (dots != 0)
    {
      ShowDots(dots, digits);
    }
  }

  TM1637_SetSegments(digits, length, pos);
}

/**
 * @brief  Create ~6 us delay.
 *         Minimum delay has to be 1 us.
 */
void BitDelay()
{
  for (volatile uint32_t d = 0; d < 10; d++);
}

void Start()
{
  SDA(0);
  BitDelay();
}

void Stop()
{
  SDA(0); BitDelay();
  SCL(1); BitDelay();
  SDA(1); BitDelay();
}

uint8_t WriteByte(uint8_t b)
{
  // 8 Data Bits (LSB first)
  for (uint8_t i = 0; i < 8; i++)
  {
    SCL(0);        BitDelay();
    SDA(b & 0x01); BitDelay();
    SCL(1);        BitDelay();

    b = b >> 1;
  }

  // Wait for acknowledge
  SCL(0); BitDelay();
  SDA(1); BitDelay();
  SCL(1); BitDelay();

  // Check acknowledge
  GPIO_PinState ack = HAL_GPIO_ReadPin(DISP_SDA_GPIO_Port, DISP_SDA_Pin);

  // Finish acknowledge
  SCL(0); BitDelay();

  return ((ack == GPIO_PIN_RESET) ? 1 : 0);
}

void ShowDots(uint8_t dots, uint8_t* digits)
{
  for(int i = 0; i < 4; ++i)
  {
    digits[i] |= (dots & 0x80);
    dots <<= 1;
  }
}

