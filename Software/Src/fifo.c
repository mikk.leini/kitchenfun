/**
  ******************************************************************************
  * @file    fifo.c
  * @author  Mikk Leini
  * @version V1.0
  * @date    22-July-2013
  * @brief   This file contains generic FIFO functions. This file has to be
  *          included from some source file.
  ******************************************************************************  
  */

/* Public functions ----------------------------------------------------------*/

/**
  * @brief  FIFO initialization
  * @param  Fifo: FIFO instance
  * @param  Data: Pointer to FIFO data
  * @param  Capacity: FIFO capacity (need to match with data count)
  */
void FIFO_FUNC(Init)(FIFO_NAME * Fifo, FIFO_TYPE * Data, U32 Capacity)
{
  FIFO_ATOMIC_START
  
  /* Preset data and size and "clear" FIFO */
  Fifo->Data       = Data;
  Fifo->Capacity   = Capacity;
  Fifo->Length     = 0U;
  Fifo->WriteIndex = 0U;
  Fifo->ReadIndex  = 0U;
  Fifo->PeekDepth  = 0U;
  
  FIFO_ATOMIC_END
}

/**
  * @brief  Clears the FIFO  
  * @param  Fifo: FIFO instance
  */
void FIFO_FUNC(Clear)(FIFO_NAME * Fifo)
{
  FIFO_ATOMIC_START
  
  Fifo->Length     = 0U;
  Fifo->WriteIndex = 0U;
  Fifo->ReadIndex  = 0U;
  Fifo->PeekDepth  = 0U;

  FIFO_ATOMIC_END
}

/**
  * @brief  Checks if FIFO is empty
  * @param  Fifo: FIFO instance
  * @retval True when FIFO is empty, False otherwise.
  */
Bool FIFO_FUNC(IsEmpty)(FIFO_NAME * Fifo)
{
  Bool result = False;

  FIFO_ATOMIC_START

  if (Fifo->Length == 0U)
  {
    result = True;
  }
  
  FIFO_ATOMIC_END
  
  return result;
}

/**
  * @brief  Checks if FIFO is full
  * @param  Fifo: FIFO instance
  * @retval True when FIFO is full, False otherwise.
  */
Bool FIFO_FUNC(IsFull)(FIFO_NAME * Fifo)
{
  Bool result = False;
    
  FIFO_ATOMIC_START
    
  if (Fifo->Length == Fifo->Capacity)
  {
    result = True;
  }
  
  FIFO_ATOMIC_END
  
  return result;
}

/**
  * @brief  Getting used space of FIFO
  * @param  Fifo: FIFO instance
  * @retval Number of used slots
  */
U32 FIFO_FUNC(GetUsedSpace)(FIFO_NAME * Fifo)
{
  U32 result = 0U;
  
  FIFO_ATOMIC_START
  result = Fifo->Length;
  FIFO_ATOMIC_END
  
  return result;
}

/**
  * @brief  Getting free space of FIFO
  * @param  Fifo: FIFO instance
  * @retval Number of free slots
  */
U32 FIFO_FUNC(GetFreeSpace)(FIFO_NAME * Fifo)
{
  U32 result = 0U;

  FIFO_ATOMIC_START
  result = Fifo->Capacity - Fifo->Length;
  FIFO_ATOMIC_END
  
  return result;
}

/**
  * @brief  Pushing item into FIFO
  * @param  Fifo: FIFO instance
  * @param  Item: Item value
  * @retval True when successful
  */
Bool FIFO_FUNC(Push)(FIFO_NAME * Fifo, FIFO_TYPE Item)
{
  Bool result = False;
  
  FIFO_ATOMIC_START
  
  /* Can't push to full FIFO */
  if (Fifo->Length < Fifo->Capacity)
  {  
    /* Write item and increase write pointer */  
    Fifo->Data[Fifo->WriteIndex] = Item;
    Fifo->WriteIndex = (Fifo->WriteIndex + 1U) % Fifo->Capacity;
    
    /* Increase count */
    Fifo->Length++;
    
    /* Done */
    result = True;
  }
  
  FIFO_ATOMIC_END

  return result;
}

/**
  * @brief  Pushing item by pointer into FIFO
  * @param  Fifo: FIFO instance
  * @param  Item: Item value
  * @retval True when successful
  */
Bool FIFO_FUNC(PushPtr)(FIFO_NAME * Fifo, const FIFO_TYPE * Item)
{
  Bool result = False;

  FIFO_ATOMIC_START

  /* Can't push to full FIFO */
  if (Fifo->Length < Fifo->Capacity)
  {
    /* Write item and increase write pointer */
    Fifo->Data[Fifo->WriteIndex] = *((FIFO_TYPE *)Item);
    Fifo->WriteIndex = (Fifo->WriteIndex + 1U) % Fifo->Capacity;

    /* Increase count */
    Fifo->Length++;

    /* Done */
    result = True;
  }

  FIFO_ATOMIC_END

  return result;
}

/**
  * @brief  Begin pushing item into FIFO
  * @param  Fifo: FIFO instance
  * @param  Item: Pointer to item pointer
  * @retval True when successful
  */
Bool FIFO_FUNC(PushBegin)(FIFO_NAME * Fifo, FIFO_TYPE ** Item)
{
  Bool result = False;

  FIFO_ATOMIC_START

  /* Can't push to full FIFO */
  if (Fifo->Length < Fifo->Capacity)
  {
    /* Get pointer to write item */
    *Item = &Fifo->Data[Fifo->WriteIndex];

    /* Done */
    result = True;
  }

  FIFO_ATOMIC_END

  return result;
}

/**
  * @brief  End pushing item into FIFO
  * @param  Fifo: FIFO instance
  * @remark This function shall only be called when PushBegin was called successfully
  */
void FIFO_FUNC(PushEnd)(FIFO_NAME * Fifo)
{
  FIFO_ATOMIC_START

  /* Increase write pointer */
  Fifo->WriteIndex = (Fifo->WriteIndex + 1U) % Fifo->Capacity;

  /* Increase count */
  Fifo->Length++;

  FIFO_ATOMIC_END
}

/**
  * @brief  Popping item from FIFO
  * @param  Fifo: FIFO instance
  * @param  Item: Pointer to item variable
  * @note   Peek pointer is resetted with popping.
  * @retval True when successful
  */
Bool FIFO_FUNC(Pop)(FIFO_NAME * Fifo, FIFO_TYPE * Item)
{
  Bool result = False;
  
  FIFO_ATOMIC_START
  
  /* Can't pop from empty FIFO */
  if (Fifo->Length > 0U)
  {
    /* Read item and increase read pointer */  
    *Item = Fifo->Data[Fifo->ReadIndex];
    Fifo->ReadIndex = (Fifo->ReadIndex + 1U) % Fifo->Capacity;
    
    /* Reset peek depth */
    Fifo->PeekDepth = 0U;

    /* Decrease count */
    Fifo->Length--;
    
    /* Done */
    result = True;
  }
    
  FIFO_ATOMIC_END

  return result;
}


/**
  * @brief  Begin popping item from FIFO
  * @param  Fifo: FIFO instance
  * @param  Item: Pointer to item pointer
  * @retval True when successful
  */
Bool FIFO_FUNC(PopBegin)(FIFO_NAME * Fifo, FIFO_TYPE ** Item)
{
  Bool result = False;

  FIFO_ATOMIC_START

  /* Can't pop from empty FIFO */
  if (Fifo->Length > 0U)
  {
    /* Get pointer to item */
    *Item = &Fifo->Data[Fifo->ReadIndex];

    /* Done */
    result = True;
  }

  FIFO_ATOMIC_END

  return result;
}

/**
  * @brief  End popping item from FIFO
  * @param  Fifo: FIFO instance
  * @remark This function shall only be called when PopBegin was called successfully
  */
void FIFO_FUNC(PopEnd)(FIFO_NAME * Fifo)
{
  FIFO_ATOMIC_START

  /* Increase read pointer */
  Fifo->ReadIndex = (Fifo->ReadIndex + 1U) % Fifo->Capacity;

  /* Decrease count */
  Fifo->Length--;

  FIFO_ATOMIC_END
}

/**
  * @brief  Returns first poppable FIFO item (if FIFO not empty)
  * @param  Fifo: FIFO instance
  * @param  Item: Pointer to item variable
  * @retval True when item exists, False if not
  */
Bool FIFO_FUNC(Front)(FIFO_NAME * Fifo, FIFO_TYPE * Item)
{
  Bool result = False;
  
  FIFO_ATOMIC_START
  
  /* Can't get item from empty FIFO */
  if (Fifo->Length > 0U)
  { 
    *Item = Fifo->Data[Fifo->ReadIndex];
    result = True;
  }
  
  FIFO_ATOMIC_END
  
  return result;
}

/**
  * @brief  Returns last pushed FIFO item (if FIFO not empty)
  * @param  Fifo: FIFO instance
  * @param  Item: Pointer to item variable
  * @retval True when item exists, False if not
  */
Bool FIFO_FUNC(Back)(FIFO_NAME * Fifo, FIFO_TYPE * Item)
{
  Bool result = False;
  
  FIFO_ATOMIC_START
  
  /* Can't get item from empty FIFO */
  if (Fifo->Length > 0U)
  { 
    *Item = Fifo->Data[Fifo->WriteIndex];
    result = True;
  }
  
  FIFO_ATOMIC_END
  
  return result;  
}

/**
  * @brief  Peeking a FIFO item
  * @param  Fifo: FIFO instance
  * @param  Item: Pointer to item variable
  * @note   Peek pointer is resetted with popping.
  * @retval True when item was peeked, False if not
  */
Bool FIFO_FUNC(Peek)(FIFO_NAME * Fifo, FIFO_TYPE * Item)
{
  Bool result = False;
  
  FIFO_ATOMIC_START
  
  /* Can't peek deeper than FIFO length is */
  if (Fifo->PeekDepth < Fifo->Length)
  {
    /* Read item at specified depth */
    *Item = Fifo->Data[(Fifo->ReadIndex + Fifo->PeekDepth) % Fifo->Capacity];

    /* Increase peek depth */
    Fifo->PeekDepth++;
    
    /* Done */
    result = True;
  }
    
  FIFO_ATOMIC_END

  return result;
}

/**
  * @brief  Restarting peeking.
  * @param  Fifo: FIFO instance
  * @retval None
  */
void FIFO_FUNC(RestartPeek)(FIFO_NAME * Fifo)
{  
  FIFO_ATOMIC_START
    
  Fifo->PeekDepth = 0U;
    
  FIFO_ATOMIC_END
}

/**
  * @brief  Flushing all already peeked items.
  * @param  Fifo: FIFO instance
  * @retval None
  */
void FIFO_FUNC(FlushAllPeeked)(FIFO_NAME * Fifo)
{
  FIFO_ATOMIC_START
  
  /* Can't flush if haven't peeked */
  if (Fifo->PeekDepth > 0U)
  {
    /* Move read index by peek depth */
    Fifo->ReadIndex = (Fifo->ReadIndex + Fifo->PeekDepth) % Fifo->Capacity;    
    
    /* Decrease size by peek depth */
    Fifo->Length -= Fifo->PeekDepth;
    
    /* Reset peek depth */
    Fifo->PeekDepth = 0U;
  }

  FIFO_ATOMIC_END
}

