/**
  ******************************************************************************
  * @file    dfplayer.h
  * @brief   This file contains functions for controlling DFPLayer mini
  * @details Based on these source codes:
  *          https://github.com/DFRobot/DFPlayer-Mini-mp3
  *          https://github.com/PowerBroker2/DFPlayerMini_Fast
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
#include "dfplayer.h"
#include "stm32l0xx.h"
#include "bytefifo.h"
#include "util.h"

/* Private constants ---------------------------------------------------------*/

#define CMD_LENGTH 10

#define SB       0x7E  // start byte
#define VER      0xFF  // version
#define LEN      0x6   // number of bytes after "LEN" (except for checksum data and EB)
#define FEEDBACK 0     // no feedback requested
#define EB       0xEF  // end byte

#define PLAY_COMMAND    0x03
#define VOLUME_COMMAND  0x06
#define EQ_COMMAND      0x07
#define LOOP_COMMAND    0x08
#define RESET_COMMAND   0x0C
#define RESUME_COMMAND  0x0D
#define PAUSE_COMMAND   0x0E

/* Public variables ----------------------------------------------------------*/
extern UART_HandleTypeDef huart1;

/* Private variables ---------------------------------------------------------*/
static U32 lastCommandTime;
static U32 lastCommandDelay;
static U16 setVolume;
static U8  packet[CMD_LENGTH];
static U8  playFifoData[8U];
static ByteFifo playFifo;

/* Private function declarations ---------------------------------------------*/
static void DFPlayer_SendCommand(U8 command, U16 param);

/* Private functions ---------------------------------------------------------*/

/**
 * @brief  Send command
 */
static void DFPlayer_SendCommand(U8 command, U16 param)
{
  U16 checksum = 0;
  U32 i;

  /* Fill packet payload */
  packet[0] = SB;
  packet[1] = VER;
  packet[2] = LEN;
  packet[3] = command;
  packet[4] = FEEDBACK;
  packet[5] = (param >> 8) & 0xFF;
  packet[6] = param & 0xFF;

  /* Calculate packet checksum */
  for (i = 1; i < 7; i++)
  {
    checksum += packet[i];
  }

  checksum = (~checksum) + 1;

  /* Create the rest of the packet */
  packet[7] = (checksum >> 8) & 0xFF;
  packet[8] = checksum & 0xFF;
  packet[9] = EB;

  /* Send packet. It should take ~10.4 ms */
  HAL_UART_Transmit(&huart1, packet, CMD_LENGTH, 15);

  /* Remember last command time */
  lastCommandTime = HAL_GetTick();
}

/* Public functions ----------------------------------------------------------*/

/**
 * @brief  Initialize DFPlayer mini
 */
void DFPlayer_Init()
{
  lastCommandTime = 0;
  lastCommandDelay = 50;
  setVolume = 0xFFFF;

  /* Create play file FIFOs */
  ByteFifo_Init(&playFifo, playFifoData, COUNT_OF(playFifoData));

  /* Reset */
  DFPlayer_SendCommand(RESET_COMMAND, 0);
}

/**
 * @brief  DFPlayer mini task
 */
void DFPlayer_Task()
{
  U8 fileIndex;

  /* Enough time (50 ms) elapsed from last command ? */
  if ((HAL_GetTick() - lastCommandTime) > lastCommandDelay)
  {
    /* Need to change volume ? */
    if (setVolume != 0xFFFF)
    {
      DFPlayer_SendCommand(VOLUME_COMMAND, setVolume);
      setVolume = 0xFFFF;
      lastCommandDelay = 50;
    }
    /* Need to play file ? */
    else if (ByteFifo_Pop(&playFifo, &fileIndex))
    {
      DFPlayer_SendCommand(PLAY_COMMAND, fileIndex);
      lastCommandDelay = 1500;
    }
    else
    {
      /* Do nothing */
    }
  }
}

/**
 * @brief  Set volume
 * @param  volume: 0 to 30 (including)
 */
void DFPlayer_SetVolume(U8 volume)
{
  if (volume > 30) volume = 30;

  setVolume = volume;
}

/**
 * @brief  Play file
 * @param  file: Zero indexed file
 */
void DFPlayer_Play(U16 file)
{
  ByteFifo_Push(&playFifo, 1 + file);
}

