/**
  ******************************************************************************
  * @file    util.c
  * @author  TUT Robotics Club NPO
  * @version V1.0
  * @date    18-August-2013
  * @brief   This file contains utility functions.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "typedefs.h"
#include "util.h"

/* Private macros ------------------------------------------------------------*/
#define IS_NUMBER(ch)  (((ch) >= '0') && ((ch) <= '9'))
#define IS_HEX(ch)     ((((ch) >= '0') && ((ch) <= '9')) || (((ch) >= 'A') && ((ch) <= 'F')))
#define HEX_TO_DEC(ch) (((ch) >= '0') && ((ch) <= '9') ? ((ch) - '0') : (     \
                        ((ch) >= 'A') && ((ch) <= 'F') ? ((ch) - 'A') : 0 ))

/* Public constants ----------------------------------------------------------*/
/* Private constants ----------------------------------------------------------*/
const U8 Util_HexDigits[16] =
{
    '0', '1', '2', '3', '4', '5', '6', '7',
    '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};


/* Private functions ---------------------------------------------------------*/
/* Public functions ----------------------------------------------------------*/


/**
  * @brief  Countdown function
  * @retval True if countdown has reached zero
  */
Bool Util_Countdown(U32 * Timer, U32 Period, Bool DetectEdge)
{
  Bool result = False;

  /* Timer still running ? */
  if (*Timer > 0UL)
  {
    if (*Timer > Period)
    {
      *Timer -= Period;
    }
    else
    {
      *Timer = 0UL;
      result = True;
    }
  }
  else
  {
    if (!DetectEdge)
    {
      result = True;
    }
  }

  return result;
}


/**
  * @brief  Date and time packing to DateTime structure.
  * @param  Dt: DateTime structure
  */
void Util_CreateDateTime(DateTime * Dt, U16 Year, U8 Month, U8 Day, U8 Hour, U8 Minute, U8 Second)
{
  U16 YearOffset = MAX(2000U, Year) - 2000U;

  /* Limit with maximum possible value, not by maximum valid value */
  Dt->Year   = MIN(YearOffset, 63U);
  Dt->Month  = MIN(Month,      15U);
  Dt->Day    = MIN(Day,        31U);
  Dt->Hour   = MIN(Hour,       31U);
  Dt->Minute = MIN(Minute,     63U);
  Dt->Second = MIN(Second,     63U);
}

/**
  * @brief  Date and time structures comparing
  * @param  A: DateTime A
  * @param  B: DateTime B
  * @retval +1 if A is later than B, -1 if A is before B, 0 if A = B
  */
S32 Util_CompareDateTime(DateTime * A, DateTime * B)
{
  U32 Ai = *((U32 *)A);
  U32 Bi = *((U32 *)B);

  if (Ai > Bi)
  {
    return 1;
  }
  else if (Ai < Bi)
  {
    return -1;
  }
  else
  {
    return 0;
  }
}

/**
  * @brief  Converting 8-byte little-endian array to 16-bit unsigned integer
  * @param  Array: 8-byte array
  * @retval 16-bit unsigned integer
  */
U16 Util_BytesToUInt16(const U8 * Array)
{
  U16 value;

  value =
    ((uint16_t)Array[1] << 8U) |
    ((uint16_t)Array[0]);

  return value;
}


/**
  * @brief  Converting 8-byte little-endian array to 32-bit unsigned integer
  * @param  Array: 8-byte array
  * @retval 32-bit unsigned integer
  */
U32 Util_BytesToUInt32(const U8 * Array)
{
  U32 value;

  value =
    ((U32)Array[3] << 24U) |
    ((U32)Array[2] << 16U) |
    ((U32)Array[1] << 8U) |
    ((U32)Array[0]);

  return value;
}


/**
  * @brief  Converting 8-byte little-endian array to 64-bit unsigned integer
  * @param  Array: 8-byte array
  * @retval 64-bit unsigned integer
  */
U64 Util_BytesToUInt64(const U8 * Array)
{
  U64 value;

  value =
    ((U64)Array[7] << 56U) |
    ((U64)Array[6] << 48U) |
    ((U64)Array[5] << 40U) |
    ((U64)Array[4] << 32U) |
    ((U64)Array[3] << 24U) |
    ((U64)Array[2] << 16U) |
    ((U64)Array[1] << 8U) |
    ((U64)Array[0]);

  return value;
}


/**
  * @brief  Converting 16-bit unsigned integer to 2-byte little-endian array
  * @param  Value: 16-bit unsigned integer
  * @param  Array: 2-byte array
  */
void Util_UInt16ToBytes(U16 Value, U8 * Array)
{
  Array[1] = (Value >> 8U) & 0xFFU;
  Array[0] = Value & 0xFFU;
}


/**
  * @brief  Converting 32-bit unsigned integer to 4-byte little-endian array
  * @param  Value: 32-bit unsigned integer
  * @param  Array: 4-byte array
  */
void Util_UInt32ToBytes(U32 Value, U8 * Array)
{
  Array[3] = (Value >> 24U) & 0xFFU;
  Array[2] = (Value >> 16U) & 0xFFU;
  Array[1] = (Value >>  8U) & 0xFFU;
  Array[0] = Value & 0xFFU;
}


/**
  * @brief  Converting 64-bit unsigned integer to 8-byte little-endian array
  * @param  Value: 64-bit unsigned integer
  * @param  Array: 8-byte array
  */
void Util_UInt64ToBytes(U64 Value, U8 * Array)
{
  Array[7] = (Value >> 56U) & 0xFFU;
  Array[6] = (Value >> 48U) & 0xFFU;
  Array[5] = (Value >> 40U) & 0xFFU;
  Array[4] = (Value >> 32U) & 0xFFU;
  Array[3] = (Value >> 24U) & 0xFFU;
  Array[2] = (Value >> 16U) & 0xFFU;
  Array[1] = (Value >>  8U) & 0xFFU;
  Array[0] = Value & 0xFFU;
}


/**
 * @brief  Convert ASCII text to hexadecimals
 * @param Text
 * @param HexBuffer
 * @param HexBufferSize
 */
void Util_ASCIIToHex(const char * Text, char * HexBuffer, U32 HexBufferSize)
{
  U32 i = 0U;
  U32 j = 0U;

  if (HexBufferSize == 0U)
  {
    return;
  }

  while ((Text[i] != 0) && ((j + 2U) < HexBufferSize))
  {
    HexBuffer[j++] = Util_HexDigits[Text[i] >> 4U];
    HexBuffer[j++] = Util_HexDigits[Text[i] & 0x0F];
    i++;
  }

  HexBuffer[j++] = 0;
}


/**
 * @brief  Convert hexadecimals to ASCII text
 * @param Text
 * @param HexBuffer
 * @param HexBufferSize
 */
void Util_HexToASCII(const char * HexBuffer, char * Text, U32 TextSize)
{
  U32 i = 0U;
  U32 j = 0U;

  if (TextSize == 0U)
  {
    return;
  }

  while (IS_HEX(HexBuffer[i]) && IS_HEX(HexBuffer[i + 1U]) && ((j + 1U) < TextSize))
  {
    Text[j++] = (HEX_TO_DEC(HexBuffer[i]) << 4U) | HEX_TO_DEC(HexBuffer[i + 1U]);
    i += 2U;
  }

  Text[j++] = 0;
}
